

class Hewan{
    void kaki(int kaki){
        System.out.println("Jumlah kaki: "+ kaki);
    }
 
    void nama(String nama){
        System.out.println("Hewan ini adalah: "+ nama);
    }

    double umur(double umur){
        System.out.println("Umurnya adalah: "+ umur);

        return umur;
    }


}


public class App {
    public static void main(String[] args) throws Exception {
        Hewan kucing = new Hewan();

        kucing.kaki(4);
        kucing.nama("Kucing");
    }
}
