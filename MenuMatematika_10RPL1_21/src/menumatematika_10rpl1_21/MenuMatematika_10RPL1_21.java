/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menumatematika_10rpl1_21;

import java.util.Scanner;



/**
 *
 * @author ACER
 */
public class MenuMatematika_10RPL1_21 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner inputMenu = new Scanner(System.in);
        
        
        System.out.println("Menu Matematika");
        System.out.println("1. Bilangan Ganjil\n2. Bilangan Genap");
        System.out.println("3. Luas Persegi\n4. Luas Persegi Panjang");
        System.out.print("Masukan Pilihan : ");
        int menu = inputMenu.nextInt();
        
       
        
//        if(menu == 1){
//            Ganjil();
//        }
//        else if (menu == 2){
//            Genap();
//        }
//        else if (menu == 3){
//            persegi();
//        }
//        else if (menu == 4){
//            persegiPanjang();
//        }
//        else{
//            System.out.println("Input yang anda masukan salah!");
//        }


           switch (menu) {
            case 1:
                Ganjil();
                break;
            case 2:
                Genap();
                break;
            case 3:
                persegi();
                break;
            case 4:
                persegiPanjang();
                break;
            default:
                System.out.println("Input yang anda masukan salah!");
           }
    }
    
    public static void Ganjil(){
        Scanner batas = new Scanner(System.in);
        System.out.print("Masukan batas awal : ");
            int i = batas.nextInt();
            System.out.print("Masukan batas akhir : ");
            int a = batas.nextInt();  
            
            System.out.print("Deret Ganjil = ");
            for (;i < a; i++){
                    if(i % 2 != 0){
                        System.out.print(i+ ", "); 
                    }
                }System.out.print("...\n");
    }
    
    public static void Genap(){
        Scanner batas = new Scanner(System.in);
        System.out.print("Masukan batas awal : ");
            int i = batas.nextInt();
            System.out.print("Masukan batas akhir : ");
            int a = batas.nextInt();
            
            System.out.print("Deret Genap = ");
            for (;i < a; i++){
                    if(i % 2 == 0){
                        System.out.print(i+ ", "); 
                    }
                }System.out.println("...");
    }
    
    public static void persegi(){
        Scanner inputSisi = new Scanner(System.in);
        System.out.print("Masukan Panjang Sisi : ");
        int sisi = inputSisi.nextInt();
        int hasil = sisi * sisi;
        System.out.println("Luas Persegi adalah : "+ hasil);
    }
    
    public static void persegiPanjang(){
        Scanner inputPanjang = new Scanner(System.in);
        System.out.print("Masukan Panjang Sisi : ");
        int panjang = inputPanjang.nextInt();
        System.out.print("Masukan Lebar Sisi : ");
        int lebar = inputPanjang.nextInt();
        
        int hasil = panjang * lebar;
        System.out.println("Luas Persegi Panjang adalah : "+ hasil);
    }
    
    
    
}
