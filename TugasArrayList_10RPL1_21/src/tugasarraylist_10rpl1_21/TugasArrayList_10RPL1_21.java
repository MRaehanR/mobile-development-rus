/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasarraylist_10rpl1_21;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class TugasArrayList_10RPL1_21 {

    static ArrayList<Integer> angka = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static int pilihan;

    public static void main(String[] args) {
        menu();
    }

    static void menu() {
        System.out.println("=========Menu==========");
        System.out.println("1. Bilangan Genap");
        System.out.println("2. Bilangan Ganjil");
        System.out.print("Masukan pilihan Anda : ");
        pilihan = input.nextInt();
        System.out.println("=======================");

        switch (pilihan) {
            case 1:
                inputLimaBil();
                showAddGenap();
                break;
            case 2:
                inputLimaBil();
                showAddGanjil();
                break;
            default:
                System.out.println("Input yang Anda masukan salah!");
                break;
        }
    }

    static void inputLimaBil() {
        for (int i = 0; i < 5; i++) {
            System.out.print("Masukan angka[" + i + "] : ");
            angka.add(input.nextInt());
        }
    }

    static void showAddGenap() {
        System.out.println("\n=======================");
        System.out.println("1. Menampilkan data ");
        System.out.println("2. Menambah data");
        System.out.print("Masukan pilihan Anda : ");
        pilihan = input.nextInt();
        System.out.println("=======================");

        if (pilihan == 1) {
            System.out.print("Data Bil. Genap : ");
            for (int bilGenap : angka) {
                if (bilGenap % 2 == 0) {
                    System.out.print(bilGenap + ", ");
                }
            }
            System.out.println("...\n");

        } else if (pilihan == 2) {
            System.out.print("Berapa angka yang akan dimasukan : ");
            pilihan = input.nextInt();
            for (int i = 0; i < pilihan; i++) {
                System.out.print("Masukan angka[" + i + "] : ");
                angka.add(input.nextInt());
            }
            System.out.println("");

            System.out.print("Data Bil. Genap : ");
            for (int bilGenap : angka) {
                if (bilGenap % 2 == 0) {
                    System.out.print(bilGenap + ", ");
                }
            }
            System.out.println("...\n");

        } else {
            System.out.println("Input yang Anda masukan salah!");
        }

        System.out.println("");
        
        ulangExit();
    }

    static void showAddGanjil() {
        System.out.println("\n=======================");
        System.out.println("1. Menampilkan data ");
        System.out.println("2. Menambah data");
        System.out.print("Masukan pilihan Anda : ");
        pilihan = input.nextInt();
        System.out.println("=======================");

        if (pilihan == 1) {
            System.out.print("Data Bil. Ganjil : ");
            for (int bilGanjil : angka) {
                if (bilGanjil % 2 != 0) {
                    System.out.print(bilGanjil + ", ");
                }
            }
            System.out.println("...\n");

        } else if (pilihan == 2) {
            System.out.print("Berapa angka yang akan dimasukan : ");
            pilihan = input.nextInt();
            for (int i = 0; i < pilihan; i++) {
                System.out.print("Masukan angka[" + i + "] : ");
                angka.add(input.nextInt());
            }
            System.out.println("");

            System.out.print("Data Bil. Ganjil : ");
            for (int bilGanjil : angka) {
                if (bilGanjil % 2 != 0) {
                    System.out.print(bilGanjil + ", ");
                }
            }
            System.out.println("...\n");

        } else {
            System.out.println("Input yang Anda masukan salah!");
        }

        System.out.println("");
        
        ulangExit();
    }

    static void ulangExit() {
        System.out.println("Apakah Anda akan menjalankan program lagi?");
        System.out.print("Silakan masukan jawaban Anda (y/t): ");
        String exit = input.next();

        switch (exit) {
            case "y":
                angka.clear();
                menu();
                break;
            case "t":
                System.out.println("\nProgram berhenti");
                break;
            default:
                System.out.println("Input yang Anda masukan salah!\n");
                ulangExit();
                break;
        }
    }

}
