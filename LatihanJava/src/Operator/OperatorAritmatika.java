/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operator;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class OperatorAritmatika {
    public static void main(String[] args) {
        Scanner inputan = new Scanner(System.in);
        int bilanganPertama;
        int bilanganKedua;
        int bilanganKetiga;
      
        System.out.print("Masukan bilangan pertama : ");
        bilanganPertama = inputan.nextInt();
        
        System.out.print("Masukan bilangan kedua : ");
        bilanganKedua = inputan.nextInt();
        
//        Penjumlahan
        bilanganKetiga = bilanganPertama + bilanganKedua;
        System.out.println(bilanganPertama+" + "+bilanganKedua+" = "+bilanganKetiga );
        
        
//        Pengurangan
        bilanganKetiga = bilanganPertama - bilanganKedua;
        System.out.println(bilanganPertama+" - "+bilanganKedua+" = "+bilanganKetiga );
        
        
//        Perkalian
        bilanganKetiga = bilanganPertama * bilanganKedua;
        System.out.println(bilanganPertama+" * "+bilanganKedua+" = "+bilanganKetiga );
        
        
//        Pembagian
        bilanganKetiga = bilanganPertama / bilanganKedua;
        System.out.println(bilanganPertama+" / "+bilanganKedua+" = "+bilanganKetiga );
        
    }
    
}
