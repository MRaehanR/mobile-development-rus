/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operator;

/**
 *
 * @author ACER
 */
public class JavaOperator {
    public static void main(String[] args) {
         // Operator Aritmatika
        System.out.println("<-- Operator Aritmatika -->");
        // Modulus 
         int a = 10;
         int b = 2;
         int hasil = a % b;
         System.out.println("Hasil mod = "+ hasil);

       
        // Increment
         int angka1 = 10;
//       System.out.println("sebelum di increment = "+ angka1);
//       angka1++;
//       System.out.println("Sesudah di increment = "+ angka1);
         
         System.out.println("Sebelum di increment = "+ angka1++);
         System.out.println("Sesudah di increment = "+ angka1);
         
         
         // Decrement
         int angka2 = 5;
         System.out.println("sebelum di decrement = "+ angka2);
         angka2--;
         System.out.println("Sesudah di decrement = "+ angka2);
         
         
         
        // Assignment Operator
         int c = 10;
         System.out.println("\n<-- Assignment Operator -->");
//         c = c + 5; //dirinya sendiri ditambah 5
//         System.out.println("Cara panjang = "+ c);
         c += 5;
         System.out.println("Cara pendek = "+ c);
         
         c -= 5;
         System.out.println("Cara pendek = "+ c);
         
         c *= 5;
         System.out.println("Cara pendek = "+ c);
         
         c /= 5;
         System.out.println("Cara pendek = "+ c);
         
         
    
        // Relational Operator
        int d = 10;
        int e = 11;
        
        System.out.println("\n<-- Rational Operator -->");
        
        boolean hasil1 = d < e;
        System.out.println("Apakah 10 lebih kecil dari 11 = "+ hasil1);
        
        boolean hasil2 = d > e;
        System.out.println("Apakah 10 lebih besar dari 11 = "+ hasil2);
        
        boolean hasil3 = d >= e;
        System.out.println("Apakah 10 lebih besar dan sama dengan 11 = "+ hasil3);
        
        boolean hasil5 = d <= e;
        System.out.println("Apakah 10 lebih kecil dan sama dengan 11 = "+ hasil5);
        
        boolean hasil6 = d != e;
        System.out.println("Apakah 10 tidak sama dengan  11 = "+ hasil6);
        
        boolean hasil7 = d == e;
        System.out.println("Apakah 10 sama dengan 11 = "+ hasil7);
        
        
        // Logical Operator
        System.out.println("\n <-- Logical Operator -->");
        boolean pernyataan1 = true;
        boolean pernyataan2 = false;
        
        boolean Hasil = pernyataan1 & pernyataan2;
        System.out.println("Jika menggunakan and '&' = "+ Hasil);
        
        boolean Hasil1 = pernyataan1 | pernyataan2;
        System.out.println("Jika menggunakan atau '|' = ");
    }
}
