/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Array {
    public static void main(String[] args) {
        Scanner inp = new Scanner (System.in);
        
        String[] makanan = new String[5];
        
        
        // Forloop
//        for (int i = 0; i < makanan.length; i++) {            
//            System.out.print("Masukan makanan ke-"+ i + " : ");
//            makanan[i] = inp.next();                                    
//        }
//        
//        for (int x = 0; x < makanan.length; x++) {
//            System.out.println(makanan[x]);
//            
//        }
        

        // While
//        int i = 0;
//        while (i < makanan.length){
//            System.out.print("Masukan makanan ke-"+ i + " : ");
//            makanan[i] = inp.next();
//            i++;
//        }
//        
//        int x = 0;
//        while (x < makanan.length){
//            System.out.println(makanan[x]);
//            x++;
//        }

          

           
        // Do While
        int i = 0;
        do {
            System.out.print("Masukan makanan ke-"+ i + " : ");
            makanan[i] = inp.next();
            i++;
        }while(i < makanan.length);
        
        
        int x = 0;
        do {
            System.out.println(makanan[x]);
            x++;
        }while(x < makanan.length);
           
        
        
    }
    
}
