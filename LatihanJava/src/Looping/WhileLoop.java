/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Looping;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class WhileLoop {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Masukan Batas awal : ");
        int awal = input.nextInt();
        System.out.print("Masukan batas akhir : ");
        int akhir = input.nextInt();
        
        System.out.print("Deret = ");
        while(awal <= akhir){
            if (awal % 2 == 0){
                System.out.print(awal+ ", ");
            }awal++;            
        }System.out.println("...");
    }
    
}
