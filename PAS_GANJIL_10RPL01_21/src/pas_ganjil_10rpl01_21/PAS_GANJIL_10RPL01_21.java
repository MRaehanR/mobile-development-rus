/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pas_ganjil_10rpl01_21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class PAS_GANJIL_10RPL01_21 {

    /**
     * @param args the command line arguments
     */
    static ArrayList<String> nama = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static Scanner inputNama = new Scanner(System.in);
    static int pilihan, no;

    public static void main(String[] args) {        
        menu();
    }

    static void menu() {
        System.out.println("=========Menu==========");
        System.out.println("1. SuperHero Marvel");
        System.out.println("2. SuperHero DC");
        System.out.print("Masukan pilihan Anda : ");
        pilihan = input.nextInt();
        System.out.println("=======================");

        switch (pilihan) {
            case 1:
                inputLimaNama();
                break;
            case 2:
                inputLimaNama();
                break;
            default:
                System.out.println("Input yang Anda masukan salah!");
                break;
        }
    }

    static void inputLimaNama() {
        for (int i = 0; i < 5; i++) {
            System.out.print("Masukan nama SuperHero: ");
            nama.add(inputNama.nextLine());
        }
        showAddData();
    }

    static void showAddData() {
        System.out.println("\n=======================");
        System.out.println("1. Menampilkan data ");
        System.out.println("2. Menambah data");
        System.out.print("Masukan pilihan Anda : ");
        pilihan = input.nextInt();
        System.out.println("=======================");

                
        if (pilihan == 1) {//data akan ditampilkan tanpa diurutkan terlebih dahulu, sesuai dgn intruksi yg ditulis
//            Collections.sort(nama); //jika ingin diurutkan terlebih dahulu maka uncomment syntax disamping
            no = 1;           
            for (String namaHero : nama) {
                System.out.println("|" + no++ + "| " + namaHero);
            }
        } else if (pilihan == 2) {
            System.out.print("Berapa nama yang akan dimasukan : ");
            pilihan = input.nextInt();

            for (int i = 0; i < pilihan; i++) {
                System.out.print("Masukan nama SuperHero: ");
                nama.add(inputNama.nextLine());
            }
            System.out.println("");
            
            Collections.sort(nama);
            no = 1;            
            for (String namaHero : nama) {//data akan ditampilkan dgn diurutkan terlebih dahulu, sesuai dgn intruksi yg ditulis
                System.out.println("|" + no++ + "| " + namaHero);
            }
        }
        ulangExit();

    }

    static void ulangExit() {
        System.out.println("\nApakah Anda akan menjalankan program lagi?");
        System.out.print("Silakan masukan jawaban Anda (y/t): ");
        String exit = input.next();

        switch (exit.toLowerCase()) {
            case "y":
                System.out.println("");
                nama.clear();                
                menu();
                break;
            case "t":
                System.out.println("\nProgram berhenti");
                break;
            default:
                System.out.println("Input yang Anda masukan salah!\n");
                ulangExit();
                break;
        }        
    }

}
