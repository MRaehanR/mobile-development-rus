/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasarray_10rpl1_21;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class TugasArray_10RPL1_21 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here        
        Scanner input = new Scanner(System.in); // input user dan pass
        Scanner inMenu = new Scanner(System.in); // input jumlah menu
        int a = 1; // untuk nomor menu yg ditampilkan
        int jumlahMenu; // jumlah menu pada Array
        int z = 1; // untuk nomor menu yg dimasukan
        
        // Memasukan Username dan Password
        System.out.println("===========Login=============");
        System.out.print("Masukan Username anda = ");
        String user = input.next(); 
        System.out.print("Masukan Password anda = ");
        String pass = input.next();
        System.out.println("=============================\n");
                
        
        if ("admin".equals(user) && "admin".equals(pass)){ // Membandingkan user dan pass apakah inputnya "admin"
            System.out.print("Hari ini ingin jualan berapa menu makanan = ");
            jumlahMenu = input.nextInt(); 
            String[] makanan = new String[jumlahMenu]; // Membuat Array dengan jumlah Array sama dengan inputan jumlahMenu
            
            System.out.println("\n======Input Menu Makanan======");
            for (int i = 0; i < makanan.length; i++) {                 
                System.out.print("Masukan menu makanan ke-"+ z++ + " = ");
                makanan[i] = inMenu.nextLine();                 
            }      
            
            System.out.println("\n======Daftar Menu Makanan======");
            System.out.println("| NO | Nama Makanan");
            
              
             // Menampilkan data yg sudah dimasukan ke Array        
           for (String Outmenu : makanan){
               System.out.println("| "+ a++ + "  | "+ Outmenu);
           }
                        
        }else { // Jika input user dan pass bukan "admin"
            System.out.println("Username atau Password yang anda masukan salah!");
        }                        
    }    
}
